<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

///  Fichier produit par PlugOnet
// Module: paquet-photo_infos
// Langue: fr
// Date: 14-11-2011 09:27:17
// Items: 3

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// P
	'portfolio_simple_v2_description' => 'Un plugin permettant la mise en œuvre rapide et simple d\'un portfolio ou d\'un book.

	Il suffit simplement de déposer les images dans une rubrique, tout le reste (titre, description, …) étant optionnels',
	'portfolio_simple_v2_nom' => 'Portfolio simple v2',
	'portfolio_simple_v2_slogan' => 'Mettez vos images dans une rubrique, et hop : voilà votre book !',
);
?>